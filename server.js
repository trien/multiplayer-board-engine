let express = require('express');
let path = require('path');
let app = express();
let http = require('http').Server(app);
let io = require('socket.io')(http);
let { dataEvents } = require('./src/common.js');
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/dist/index.html');
});
app.use('/', express.static(path.join(__dirname + '/')));

let sessions = {},
    sockets = {},
    sessionI = 0,
    emitToSession = (sessionId, key, data) => {
        if (sessions[sessionId]) {
            sessions[sessionId].players.forEach(p => {
                console.log(`Session ${sessionId} Emit ${key} for player ${p.id}`);
                sockets[p.id].emit(key, data);
            });
        }
    };

io.on('connection', socket => {
    let player = { id: socket.id };
    sockets[socket.id] = socket;
    console.log(`user ${socket.id} connected`);
    socket.on('disconnect', () => {
        delete sockets[socket.id];
        for (let id in sessions) {
            let playerI = sessions[id].players.findIndex(p => p.id === socket.id);
            if (playerI !== -1) {
                sessions[id].players.splice(playerI, 1);
                io.emit(dataEvents.PLAYER.DESTROY, sessions[id]);
                console.log(`player ${socket.id} disconnected`);
            }
        }
    })
    .on(dataEvents.GAME.LOAD, () => {
        socket.emit(dataEvents.GAME.LOAD, sessions);
        console.log(`game sessions loaded`);
    })
    .on(dataEvents.GAME.CREATE, () => {
        let newSession = sessions[++sessionI] = {
            id: sessionI,
            players: []
        };
        io.emit(dataEvents.GAME.CREATE, newSession);
        console.log(`game session ${sessionI} created`);
    })
    .on(dataEvents.GAME.DESTROY, sessionId => {
        delete sessions[sessionId];
        io.emit(dataEvents.GAME.DESTROY, sessionId);
        console.log(`game session ${sessionId} closed`);
    })
    .on(dataEvents.PLAYER.CREATE, sessionId => {
            let session = sessions[sessionId];
            session.players.push(player);
            io.emit(dataEvents.GAME.NEW_PLAYER, session);
            socket.emit(dataEvents.PLAYER.CREATE, player.id);
            console.log(`game session ${sessionId} : player ${player.id} created`);
        })
    .on(dataEvents.PLAYER.MOVE, (sessionId, {index, x, y}) => {
        emitToSession(sessionId, dataEvents.PLAYER.MOVE, {index, x, y});
        console.log(`game session ${sessionId} : player ${index} move to ${x}/${y}`);
    });
});

http.listen(3000, () => {
    console.info('listening on *:3000');
});
