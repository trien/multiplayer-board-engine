let flowEvents = {
    CELL: {
        NEW_CHILDREN: 'CELL_NEW_CHILDREN',
        REMOVE_CHILDREN: 'CELL_REMOVE_CHILDREN',
    },
    PLAYER: {
        CREATE: 'PLAYER_CREATE',
        DESTROY: 'PLAYER_DESTROY',
        MOVE: 'PLAYER_MOVE',
        LEAVE: 'PLAYER_LEAVE',
        TURN: {
            START: 'PLAYER_TURN_START',
            END: 'PLAYER_TURN_END'
        },
    }
},
dataEvents = {
    REMOTE: {
        RELOAD: 'REMOTE_RELOAD',
    },
    GAME: {
        CREATE: 'GAME_CREATE',
        DESTROY: 'GAME_DESTROY',
        LOAD: 'GAME_LOAD',
        NEW_PLAYER: 'GAME_NEW_PLAYER'
    },
    PLAYER: {
        CREATE: flowEvents.PLAYER.CREATE,
        DESTROY: flowEvents.PLAYER.DESTROY,
        MOVE: flowEvents.PLAYER.MOVE,
    }
};

module.exports.flowEvents = flowEvents;
module.exports.dataEvents = dataEvents;
