import * as bus from './EventBus';

export default class Player {
    constructor(id, index, playable) {
        this.id = id;
        this.index = index;
        this.playable = playable;
        this.node = document.createElement('div');
        this.node.id = `player${this.index}`;
        this.node.classList.add('autocenter', 'player');
        this.name = `Player ${this.index+1}`;

        this.moveDistance = 10;
        this.node.onclick = e => {
            e.stopPropagation();
            bus.interact(this, e);
        };
        this.init();
    }

    init() {
        this.setPosition(0, this.index);
    }

    setPosition(x, y) {
        this.x = x;
        this.y = y;
    }

    destroy() {
        this.node.remove();
        bus.flow(this, bus.flowEvents.PLAYER.DESTROY);
    }
}