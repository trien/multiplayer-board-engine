import { createStore } from 'redux';
import { flowEvents, dataEvents } from '../common';
import io from 'socket.io-client';

export { store, flowEvents, dataEvents, interact, flow, setScene, socket }

let actions = {
        INTERACTIVE: 'INTERACTIVE',
        FLOW: 'FLOW',
    },
    store = createStore((state = 0, action) => {
        console.info(action);
        switch (action.type) {
            case actions.INTERACTIVE:
                if (action.target.constructor.name)
                    action.objectType = action.target.constructor.name;
                return action;
            case actions.FLOW:
                if (action.target) {
                    action.objectType = action.target.constructor.name;
                }
                return action;
            default:
                return state
        }
    }),
    /**
     *
     * @param target: Object
     * @param event: Event
     */
    interact = (target, event) => {
        store.dispatch({ type: actions.INTERACTIVE, target, event });
    },
    /**
     *
     * @param target: Object
     * @param event: String
     */
    flow = (target, event) => {
        store.dispatch({type: actions.FLOW, target, event: {type: event }})
    },
    /**
     *
     * @param {String} scene
     * @param data
     */
    setScene = (scene, data = {}) => {
        store.dispatch({type: 'FLOW', objectType: 'Scene', scene: scene, data });
    },
    socket = io('http://localhost:3000')
;
socket.on('disconnect', function () {
    location.reload();
});