import Cell from './Cell';

export default class Playground {
    constructor({   wrap = document.body,
                    with_border = true,
                    cellNb = 10,
                    map
                }) {
        this.cellNb = cellNb;
        this.wrap = wrap;
        this.node = document.createElement('section');
        this.node.id = 'playground';
        this.node.classList.add('autocenter');
        if (with_border) this.node.classList.add('with_border');
        this.wrap.appendChild(this.node);

        if (!map)
            map = Array(cellNb).fill().map(() =>
                Array(cellNb).fill().map(() => null)
            );
        this.map = map;
        this.initMap();

        let style = document.createElement('style');
        document.body.appendChild(style);
        style.innerText = `i {` +
            `width: ${100 / this.cellNb}%;` +
            `height: ${100 / this.cellNb}%;` +
        `}`;
    }

    initMap() {
        let cells = [];
        this.map.forEach((row, x) => {
            cells[x] = [];
            row.forEach((cellType, y) => {
                let cell = cells[x][y] = new Cell(x, y, cellType);
                this.node.appendChild(cell.node);
            });
        });
        this.cells = cells;
    }

    show() {
        this.wrap.classList.remove('hidden');
    }

    exportMap() {
        return JSON.stringify(this.cells.map(row => row.map(c => c.node.classList.toString())));
    }

    findCell(x, y) {
        return this.cells[x] && this.cells[x][y];
    }

    getCell(x, y) {
        return this.findCell(x, y) ? this.cells[x][y] : false;
    }

    /**
     * @param type
     */
    getCellOfType(type) {
        return this.cells.filter(row => row.filter(c => c.is(type)));
    }
    /**
     * @param type
     */
    removeTypeCells(type) {
        return this.getCellOfType().map(row => row.map(c => c.removeType(type)));
    }

    setCellChildren(element, x, y) {
        let canSet = this.findCell(x, y);
        if (canSet) {
            let cell = this.cells[x][y];
            cell.addElement(element);
            canSet = true;
        }
        return canSet;
    }
}