import Scene from '../Scene';
import * as bus from '../EventBus';
import Game from './Game';
import Player from '../Player';
export default class Matchmaking extends Scene {
    constructor() {
        super();
        this.sessions = {};
        this.gameSession = null;
        this.playerId = null;

        this.newGameButton = this.node.querySelector('#new_game');
        this.gamesList = this.node.querySelector('#games tbody');

        this.newGameButton.addEventListener('click', () => {
            bus.socket.emit(bus.dataEvents.GAME.CREATE);
        });
    }

    load(playerId) {
        super.load();
        this.sessions = {};
        this.gameSession = null;
        this.playerId = playerId;

        bus.socket.emit(bus.dataEvents.GAME.LOAD)
    }

    addGame(session) {
        let id = session.id;
        this.sessions[id] = session;
        this.sessions[id].players = session.players;

        let joinRow = document.createElement('tr');
        joinRow.id = 'session_'+id;

        let name = document.createElement('td');
        name.innerText = '#'+id;
        joinRow.appendChild(name);

        let playerList = document.createElement('td');
        playerList.classList.add('player_list');
        joinRow.appendChild(playerList);

        let join = document.createElement('td'),
            joinButton = document.createElement('button'),
            joinWait = document.createElement('span'),
            joinFull = document.createElement('span');
        join.classList.add('join_game');
        joinButton.innerText = 'JOIN';
        joinButton.sessionId = id;
        joinButton.addEventListener('click', e => {
            let sessionId = e.target.sessionId;
            this.gameSession = sessionId;
            bus.socket.emit(bus.dataEvents.PLAYER.CREATE, sessionId);

            let buttons = this.node.querySelectorAll('.join_game button');
            for (let b in buttons) { buttons.item(b).classList.add('hidden'); }
            this.node.querySelector(`#session_${sessionId} .wait`).classList.remove('hidden');
            this.newGameButton.classList.add('hidden');
        });
        joinWait.innerText = 'WAITING...';
        joinWait.classList.add('wait');
        joinWait.classList.add('hidden');
        joinFull.innerText = 'FULL';
        joinFull.classList.add('full');
        joinFull.classList.add('hidden');
        join.appendChild(joinButton);
        join.appendChild(joinWait);
        join.appendChild(joinFull);
        joinRow.appendChild(join);
        this.gamesList.appendChild(joinRow);

        this.updatePlayers(id);
    }

    updateGames(sessions){
        this.gamesList.innerHTML = '';
        for (let id in sessions) {
            this.addGame(sessions[id]);
        }

        this.newGameButton.classList.toggle('hidden', sessions.length > 0);
    }

    updatePlayers(sessionId){
        let nb = this.sessions[sessionId].players.length,
            max = Game.maxPlayers,
            node = this.node.querySelector(`#session_${sessionId}`);
        node.querySelector('.player_list').innerText = `${nb}/${max}`;
        if (!this.gameSession) {
            node.querySelector('button').classList.toggle('hidden', nb === max);
            node.querySelector('.full').classList.toggle('hidden', nb !== max);
        }
    }

    startGame(id) {
        if (this.gameSession === id && this.sessions[id].players.length === Game.maxPlayers) {
            let currentPlayers = [];
            this.sessions[id].players.forEach((p, index) => {
                currentPlayers.push(new Player(p.id, index, p.id === this.playerId));
            });
            bus.setScene('game', {id, players: currentPlayers});
        }
    }
}