import Scene from '../Scene';
import * as bus from '../EventBus';
export default class Splash extends Scene {
    constructor(){
        super();
        bus.socket.on('connect', () => {
            this.playerId = bus.socket.id;
        });
    }
    load() {
        super.load();
        this.node.querySelector('#start').addEventListener('click', () => {
            bus.setScene('matchmaking', this.playerId);
        });
    }

}