import Scene from '../Scene';
import * as bus from '../EventBus';
import Playground from '../Playground';
import Player from '../Player';

export default class Game extends Scene{
    static get maxPlayers () { return 3; }
    constructor() {
        super();
        this.id = null;
        this.turnLoop = false;
        this.players = [];
        this.mapSrc = 'dist/json/defaultMap.json';
    }

    load({id, players}) {
        super.load();
        this.id = id;
        this.players = players;
        fetch(this.mapSrc)
        .then(response => response.json())
        .then(map => {
            this.map = map;
            this.init();
        }).catch(error => {
            console.error(error);
            this.map = null;
            this.init();
            this.sendMessage('Fail to load map');
        });
    }

    init() {
        this.currentPlayerId = 0;

        this.playground = new Playground({
            map: this.map,
            wrap: this.node,
        });
        this.players.forEach(p => {
            this.setPlayerPosition(p, p.x, p.y)
        });
        this.turnLoop = true;
        this.diagonalsMoves = true;
        bus.flow(this.getCurrentPlayer(), bus.flowEvents.PLAYER.TURN.START);
    }

    setPlayerPosition(player, x, y) {
        let movable = this.playground.setCellChildren(player, x, y);
        if (movable) {
            player.x = x;
            player.y = y;
        }
        return movable;
    }

    /**
     *
     * @param id
     * @returns {Player|boolean}
     */
    getPlayer(id) {
        return this.players[id] || false;
    }

    /**
     * @returns {Player|boolean}
     */
    getCurrentPlayer() {
        return this.getPlayer(this.currentPlayerId);
    }

    suggestMoves() {
        if (this.getCurrentPlayer().playable) {
            let p = this.getCurrentPlayer();
            for (let x = p.x - p.moveDistance; x <= p.x + p.moveDistance; x++) {
                for (let y = p.y - p.moveDistance; y <= p.y + p.moveDistance; y++) {
                    let cell = this.playground.getCell(x, y);
                    if (cell && cell.isEmpty() && !cell.is('wall')) {
                        if (this.diagonalsMoves || (p.x === x || p.y === y))
                            cell.setType('selectable');
                    }
                }
            }

            if (!this.playground.getCellOfType('selectable').length) {
                this.sendMessage(p.name + ' : aucun mouvements possible');
                bus.flow(p, bus.flowEvents.PLAYER.MOVE);
            }
        }
    }

    nextPlayer() {
        if (this.turnLoop) {
            this.closeMessageBox();
            bus.flow(this.getCurrentPlayer(), bus.flowEvents.PLAYER.TURN.END);
            this.currentPlayerId++;
            if (this.currentPlayerId === this.players.length) this.currentPlayerId = 0;
            if (this.getCurrentPlayer().playable) {
                this.sendMessage(`YOUR TURN`);
            }
            bus.flow(this.getCurrentPlayer(), bus.flowEvents.PLAYER.TURN.START);
        }
    }

    endTurn() {
        this.playground.removeTypeCells('selectable');
    }

    win() {
        this.endTurn();
        this.turnLoop = false;
        this.sendChoices(this.getCurrentPlayer().name + ' wins.', [
            { text: 'Ok', callback: 'LEAVE'}
        ]);
        setTimeout(() => {bus.socket.emit(bus.dataEvents.GAME.DESTROY, this.id);}, 10*1000);
    }

    openMessageBox() {
        this.node.querySelector('#toggle_message').checked = false;
    }
    closeMessageBox() {
        this.node.querySelector('#toggle_message').checked = true;
    }
    sendMessage(message) {
        this.node.querySelector('.message').innerText = message;
        this.openMessageBox();
    }
    sendChoices(message, choices) {
        this.sendMessage(message);
        let choicesWrap = this.node.querySelector('#message_box .choices');
        choicesWrap.innerHTML = '';
        choices.forEach(c => {
            let l = document.createElement('button');
            l.innerText = c.text;
            l.addEventListener('click', () => {
                switch (c.callback) {
                    case 'LEAVE':
                        bus.setScene('splash', this.playerId);
                        break;
                }
                choicesWrap.innerHTML = '';
                this.closeMessageBox();
            });
            choicesWrap.appendChild(l);
        });
    }
}