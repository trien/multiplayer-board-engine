export default class Scene {
    constructor() {
        this.node = document.querySelector('main#'+this.constructor.name.toLowerCase());
    }
    load() {
        let scenes = document.querySelectorAll('main');
        for (let i in scenes) scenes.item(i).classList.add('hidden');
        this.node.classList.remove('hidden');
    }
}