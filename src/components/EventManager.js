import * as bus from './EventBus';
import Splash from "./Scenes/Splash";
import Matchmaking from "./Scenes/Matchmaking";
import Game from "./Scenes/Game";
import Player from "./Player";

export default class EventManager {
    constructor() {
        this.splash = new Splash();
        this.matchmaking = new Matchmaking();
        this.game = new Game();
        bus.store.subscribe(() => {
            let {objectType, target, event} = bus.store.getState();
            switch (objectType) {
                case 'Player':
                    this.playerEvents(target, event);
                    break;
                case 'Cell':
                    this.cellEvents(target, event);
                    break;
                case 'Scene':
                    let {scene, data} = bus.store.getState();
                        this[scene].load(data);
                    break;

                default:
                    this.noResponseType('Object', objectType, event);
            }
        });

        bus.socket
        .on(bus.dataEvents.PLAYER.MOVE, ({index, x, y}) => {
            this.game.setPlayerPosition(this.game.getPlayer(index), x, y);
            this.game.nextPlayer();
        })
        .on(bus.dataEvents.GAME.LOAD, sessions => {
            this.matchmaking.updateGames(sessions);
        })
        .on(bus.dataEvents.GAME.CREATE, session => {
            this.matchmaking.addGame(session);
        })
        .on(bus.dataEvents.GAME.NEW_PLAYER, ({id, players}) =>{
            this.matchmaking.sessions[id].players = players;
            this.matchmaking.updatePlayers(id);
            this.matchmaking.startGame(id);
        })
        .on(bus.dataEvents.GAME.DESTROY, sessionId => {
            delete this.matchmaking.sessions[sessionId];
            this.matchmaking.updateGames(this.matchmaking.sessions);
        })
        .on(bus.dataEvents.PLAYER.DESTROY, ({id, players}) => {
            if (this.matchmaking.sessions[id]) {
                this.matchmaking.sessions[id].players = players;
                this.matchmaking.updatePlayers(id);
            }

            if (this.game.id === id) {
                this.game.sendChoices('A player leave the game. Try to create another one.', [
                    { text: 'Ok', callback: 'LEAVE'}
                ]);
            }
        });
    }

    /**
     *
     * @param {String} type
     * @param {Object} target
     * @param event
     */
    noResponseType(type, target, event) {
        console.error(type+' type not managed', target, event || 'no event');
    }

    /**
     * @param {Player} player
     * @param event
     */
    playerEvents(player, event) {
        switch (event.type) {
            case 'click':
                break;
            case bus.flowEvents.PLAYER.TURN.START:
                this.game.suggestMoves();
                break;
            case bus.flowEvents.PLAYER.MOVE:
                this.game.setPlayerPosition(player, player.x, player.y);
                this.game.nextPlayer();
                break;
            case bus.flowEvents.PLAYER.TURN.END:
                this.game.endTurn();
                break;
            default:
                this.noResponseType('Event', player, event);
        }
    }

    /**
     *
     * @param {Cell} cell
     * @param event
     */
    cellEvents(cell, event) {
        switch (event.type) {
            case 'click':
                if (cell.isEmpty() && cell.is('selectable') && !cell.is('wall')) {
                    let player = this.game.getCurrentPlayer();
                    this.game.setPlayerPosition(player, cell.x, cell.y);
                    bus.socket.emit(bus.dataEvents.PLAYER.MOVE, this.game.id, player);
                }
                break;
            case bus.flowEvents.CELL.NEW_CHILDREN:
                if (cell.is('exit')) this.game.win();
                break;
            default:
                this.noResponseType('Event', cell, event);
        }
    }
}