import * as bus from './EventBus';

export default class Cell {
    constructor(x, y, type) {
        this.x = x;
        this.y = y;
        this.node = document.createElement('i');
        if (!!type) {
            if (Array.isArray(type))
                type.forEach(t => this.setType(t));
            else
                this.node.classList.add(type);
        }
        this.node.onclick = e => {
            bus.interact(this, e);
        };
    }

    setType(type) {
        return this.node.classList.add(type);
    }
    removeType(type) {
        return this.node.classList.remove(type);
    }
    is(type) {
        return this.node.classList.contains(type);
    }

    isEmpty() {
        return !this.node.children.length;
    }

    addElement(element) {
        this.node.appendChild(element.node);
        bus.flow(this, bus.flowEvents.CELL.NEW_CHILDREN)
    }
    removeElement(element) {
        this.node.removeChild(element.node);
        bus.flow(this, bus.flowEvents.CELL.REMOVE_CHILDREN)
    }


}