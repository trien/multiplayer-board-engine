import EventManager from './components/EventManager';
import {setScene} from './components/EventBus';

new EventManager();
setScene('splash');